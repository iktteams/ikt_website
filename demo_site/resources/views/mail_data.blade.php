<link rel="stylesheet" href="<?php echo url('/'); ?>/css/bootstrap.min.css">

Hi Sir
<br>
<br>
{{ $details['title'] }}
<br>
<br>
{{ $details['body'] }}
<br>
<br>

<p>Thank you</p>


  <!-- ALL JS FILES -->
  <script src="<?php echo url('/'); ?>/js/all.js"></script>
  <!-- Camera Slider -->
  <script src="<?php echo url('/'); ?>/js/jquery.mobile.customized.min.js"></script>
  <script src="<?php echo url('/'); ?>/js/jquery.easing.1.3.js"></script>
  <script src="<?php echo url('/'); ?>/js/parallaxie.js"></script>
  <script src="<?php echo url('/'); ?>/js/headline.js"></script>
  <!-- Contact form JavaScript -->
  <script src="<?php echo url('/'); ?>/js/jqBootstrapValidation.js"></script>
  <script src="<?php echo url('/'); ?>/js/contact_me.js"></script>
  <!-- ALL PLUGINS -->
  <script src="<?php echo url('/'); ?>/js/custom.js"></script>
  <script src="<?php echo url('/'); ?>/js/jquery.vide.js"></script>
