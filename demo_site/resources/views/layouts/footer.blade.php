
    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
				<a href="#"><img class="rounded"  src="<?php echo url('/'); ?>/images/welogos.png" alt="company logo" /></a>
                <div class="footer-center">
                    <p class="footer-links">
                        <a href="#home" class="js-scroll-trigger">Home</a>
                        <a href="#blog" class="js-scroll-trigger">Blog</a>
                        {{-- <a href="#">Pricing</a> --}}
                        <a href="#about" class="js-scroll-trigger">About</a>
                        {{-- <a href="#">Faq</a> --}}
                        <a href="#contact" class="js-scroll-trigger">Contact</a>

                    </p>
                    <p class="footer-company-name">All Rights Reserved. &copy; 2020 <a href="#">IKTSS Softawre Solutions</a> Design By :
					<a href="https:www.iktss.com">Design By IKTSS</a></p>
                </div>
            </div>
        </div><!-- end container -->
    </div><!-- end copyrights -->

    <a href="#" id="scroll-to-top" class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>
