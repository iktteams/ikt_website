<div id="contact" class="section db">
    <div class="container">
        <div class="section-title text-center">
            <h3>Contact</h3>
            <p>Get in touch with us and we can help you reimagine your enterprise for the digital age.</p>
        </div><!-- end title -->




        <div class="row">
            <div class="col-md-12">
                <div class="contact_form">
                    <div id="message"></div>
                    <form name="sentMessage" id="sentMessage" method="POST">


                        <?php
                        if(isset($_POST["sendMessageButton"]))
                        {
                          $name = $_POST['name'];
                          $email = $_POST['email'];
                          $phone = $_POST['phone'];
                          $message = $_POST['message'];
                          $all="user name is ".$name." and "."email is ".$email." and "."contact is ".$phone;

                          //echo $name.$email.$phone. $message;
                          $details= [
                                    'title' => $all,
                                    'body' => "User message is ".$message
                          ];
                          $to = [
                              ['email' => 'sales@iktss.com'],
                              ['email' => 'rs@iktss.com']
                          ];
                           \Mail::to($to)->send(new \App\Mail\MyTestMail($details));

                            ?>
                            <div autofocus="true" style="margin-top: 5px"  class="alert alert-success alert-dismissible" id="success-alert" >
                              <button autofocus="true" type="button" class="close" data-dismiss="alert">&times;</button>
                              <strong>Thanks for contact us, We will contact you soon !!!</strong>
                          </div>
                            <?php
                        }
                       ?>

                        <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input class="form-control" name="name" type="text" placeholder="Your Name" required="required" data-validation-required-message="Please enter your name."
                                    oninvalid="this.setCustomValidity('Please fill name here')"
                                    oninput="this.setCustomValidity('')" onvalid="this.setCustomValidity('')">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="email" type="email" placeholder="Your Email" required="required" data-validation-required-message="Please enter your email address." oninvalid="this.setCustomValidity('Please fill email here')"
                                    oninput="this.setCustomValidity('')" onvalid="this.setCustomValidity('')">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" name="phone" type="tel" placeholder="Your Phone" required="required" data-validation-required-message="Please enter your phone number."
                                    oninvalid="this.setCustomValidity('Please fill contact no here')"
                                    oninput="this.setCustomValidity('')" onvalid="this.setCustomValidity('')">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="message" placeholder="Your Message" required="required" data-validation-required-message="Please enter a message."
                                    oninvalid="this.setCustomValidity('Please fill message here')"
                                    oninput="this.setCustomValidity('')"
                                    onvalid="this.setCustomValidity('')"
                                    ></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button id="sendMessageButton" name="sendMessageButton"
                                 class="sim-btn hvr-bounce-to-top" type="submit">Send Message</button>
                                {{-- <input type="submit" value="register"> --}}
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->
