<div id="services" class="section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Services</h3>
            <p>IKTSS is a certified website development company in india provides a wide range of website and app design services.</p>
        </div><!-- end title -->

        <div class="row">
            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-seo"></i>
                    </div>
                    <h2>Web Development</h2>
                    <p>Best Web Development Company in India, IKTSS specializes in designing effective virtual branding and giving high tech website development services. </p>
                </div>
            </div><!-- end col -->





            {{-- new code start --}}

            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-idea"></i>
                    </div>
                    <h2>Cloud Computing & DevOps</h2>
                    <p>DevOps and cloud is the combination of practices, and tools that increases an organization’s ability to deliver applications and services at high velocity.</p>
                </div>
            </div><!-- end col -->


            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-seo"></i>
                    </div>
                    <h2>Software Testing</h2>
                    <p>Software Testing provides development teams with ways and tools to determine the quality of their software.To do this automation and manual tools is use.</p>
                </div>
            </div><!-- end col -->


            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-seo"></i>
                    </div>
                    <h2>Data Analytics</h2>
                    <p>Data analysis is a process of inspecting, cleansing, transforming and modeling data with the goal of discovering useful information, informing conclusions and supporting decision-making.</p>
                </div>
            </div><!-- end col -->


            {{-- new code end --}}











            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-development"></i>
                    </div>
                    <h2>System Support</h2>
                    <p>System support specialists troubleshoot hardware problems and find fixes for them. This work involves computers, phones, safes, printers, and all digital systems connected to the network.</p>
                </div>
            </div><!-- end col -->
            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-process"></i>
                    </div>
                    <h2>Industrial Automation</h2>
                    <p>Industrial automation is the use of control devices such as PC/PLCs/PACs etc to control industrial processes and machinery by removing as much labor intervention as possible, and use automated ones.</p>
                </div>
            </div><!-- end col -->
            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-discuss-issue"></i>
                    </div>
                    <h2>Multimedia & Digital solution</h2>
                    <p>Digital solutions are more about digitalizing legacy processes revisiting customer experience and reducing costs.</p>
                </div>
            </div><!-- end col -->
            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-idea"></i>
                    </div>
                    <h2>Artificial Intelligence</h2>
                    <p>Artificial intelligence is wide-ranging branch of computer science concerned with building smart machines capable of performing tasks that typically require human intelligence.</p>
                </div>
            </div><!-- end col -->
            <div class="col-md-4">
                <div class="services-inner-box">
                    <div class="ser-icon">
                        <i class="flaticon-idea-1"></i>
                    </div>
                    <h2>Graphic Design</h2>
                    <p>Graphic design is the process of visual communication and problem-solving through the use of typography, photography, iconography and illustration.</p>
                </div>
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->
