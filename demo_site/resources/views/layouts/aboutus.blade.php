<div id="about" class="section wb">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="message-box">
                    <h2>Welcome to IKTARA</h2>
                    <p> IKTSS is a privately held company , consisting of a creative and multi talented team comprised of web designers, grpahic experts , web & mobile application developers. We provide a wide range of services including web design, web development, mobile application development, web hosting and consulting.</p>

                    <p>Our team have expertise in delivering top-notch solutions . We have a tendenacy to leverage high-end strategy utilizing our in depth talent and technology expertise.</p>

                    <p>
                        Fortunately, we have been able to gather a crew of talented professionals who will style effective solutions that can help to accelerate your oraganization.
                    </p>

                    <p>IKTSS offers customer oriented services and delivers creative and effective results.</p>

                    {{-- <a href="#contact" class="sim-btn hvr-bounce-to-top"><span>Contact Us</span></a> --}}
                    <a href="#contact" class="sim-btn js-scroll-trigger"><span>Contact Us</span></a>
                </div><!-- end messagebox -->
            </div><!-- end col -->

            <div class="col-md-6">
                <div class="right-box-pro wow fadeIn">
                    <img src="uploads/about_04.jpg" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

