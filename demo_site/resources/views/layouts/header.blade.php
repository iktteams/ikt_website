 <!-- Navigation -->
 <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">
          {{-- <img class="img-fluid" src="/images/logo.png" alt="" /> --}}
          <img class="img-fluid rounded"  src="<?php echo url('/'); ?>/images/website_logo_final.png" alt="website logo image" />
      </a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav text-uppercase ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger active" href="#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">About Us</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#mission">Mission & Vision</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#team">Our Team</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact Us</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <section id="home" class="main-banner parallaxie" style="background: url('uploads/banner-01.jpg')">
    <div class="heading">
        <h1>Welcome to <span style="text-transform: lowercase">i</span>KT Software Solutions</h1>
        <h3 class="cd-headline clip is-full-width">
            <span>A Primer Company For </span>
            <span class="cd-words-wrapper">
                <b class="is-visible">Web Developer</b>
                <b>Web Design</b>
                <b>Creative Design</b>
                <b>Graphic Design</b>
            </span>
            <div class="btn-ber">
                <a class="get_btn hvr-bounce-to-top" href="#">Get started</a>
                <a class="learn_btn hvr-bounce-to-top" href="#">Learn More</a>
            </div>
        </h3>
    </div>
</section>

