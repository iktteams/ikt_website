<div id="blog" class="section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Blog</h3>
            <p>Explore the ITKSS blog for elegant insights and thought leadership on industry best practices in information technology industry.</p>
        </div><!-- end title -->

        <div class="row">
            <div class="col-md-4 col-sm-6 col-lg-4">
                <div class="post-box">
                    <div class="post-thumb">
                        <img src="uploads/blog-01.jpg" class="img-fluid" alt="post-img" />
                        <div class="date">
                            <span>06</span>
                            <span>Aug</span>
                        </div>
                    </div>
                    <div class="post-info">
                        <h4>Making your Angular apps fast.</h4>
                        <ul>
                            <li>by admin</li>
                            <li>Apr 21, 2018</li>
                            <li>
                                <a href="#"><b> Comments</b></a>
                            </li>
                        </ul>
                        <p>We’re interested in runtime performance and how long it takes Angular to perform a task when the mousemove event has been fired, as this is the work Angular has to do when we drag and drop a box.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-4">
                <div class="post-box">
                    <div class="post-thumb">
                        <img src="uploads/blog-02.jpg" class="img-fluid" alt="post-img" />
                        <div class="date">
                            <span>20</span>
                            <span>Aug</span>
                        </div>
                    </div>
                    <div class="post-info">
                        <h4>What is REST API?.</h4>
                        <ul>
                            <li>by admin</li>
                            <li>Jan 5, 2018</li>
                            <li><a href="#"><b> Comments</b></a></li>
                        </ul>
                        <p>REST (Representational State Transfer) is an API that defines a set of functions that programmers can use to send requests and receive responses using the HTTP protocol methods such as GET and POST. </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-lg-4">
                <div class="post-box">
                    <div class="post-thumb">
                        <img src="uploads/blog-03.jpg" class="img-fluid" alt="post-img" />
                        <div class="date">
                            <span>06</span>
                            <span>Aug</span>
                        </div>
                    </div>
                    <div class="post-info">
                        <h4>Check OpenSSH version.</h4>
                        <ul>
                            <li>by admin</li>
                            <li>Apr 21, 2019</li>
                            <li><a href="#"><b> Comments</b></a></li>
                        </ul>
                        <p>On Linux, we can use ssh -v localhost or ssh -V to check the OpenSSH version currently installed.OpenSSH is integrated into several operating systems,while the portable version is available as a package in other systems.</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>













































