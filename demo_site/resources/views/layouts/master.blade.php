<!DOCTYPE html>

<html lang="en">

<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

     <!-- Site Metas -->
    <title>IKTSS - Global leader in Software Development</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="<?php echo url('/'); ?>/images/logo_real.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?php echo url('/'); ?>/images/logo_real.ico">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/bootstrap.min.css">
    <!-- Site CSS -->
    {{-- <link rel="stylesheet" href="/style.css"> --}}
	<link rel="stylesheet" href="<?php echo url('/'); ?>/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/custom.css">
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/flaticon.css">
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/font-awesome.css">
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/prettyPhoto.css">
    <link rel="stylesheet" href="<?php echo url('/'); ?>/css/camera.css">
	<script src="<?php echo url('/'); ?>/js/modernizr.js"></script> <!-- Modernizr -->

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.<?php echo url('/'); ?>/js/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>

<body id="page-top" class="politics_version">

       <!-- LOADER -->
       <div id="preloader">
        <div id="main-ld">
			<div id="loader"></div>
		</div>
    </div>
    <!-- end loader -->
    <!-- END LOADER -->

    @include('layouts.header')
    @include('layouts.aboutus')
    @include('layouts.services')
    @include('layouts.portfolio')
    @include('layouts.blog')
    @include('layouts.team')
    @include('layouts.contact')

    @include('layouts.footer')


    <!-- ALL JS FILES -->
    <script src="<?php echo url('/'); ?>/js/all.js"></script>
	<!-- Camera Slider -->
	<script src="<?php echo url('/'); ?>/js/jquery.mobile.customized.min.js"></script>
	<script src="<?php echo url('/'); ?>/js/jquery.easing.1.3.js"></script>
	<script src="<?php echo url('/'); ?>/js/parallaxie.js"></script>
	<script src="<?php echo url('/'); ?>/js/headline.js"></script>
	<!-- Contact form JavaScript -->
    <script src="<?php echo url('/'); ?>/js/jqBootstrapValidation.js"></script>
    <script src="<?php echo url('/'); ?>/js/contact_me.js"></script>
    <!-- ALL PLUGINS -->
    <script src="<?php echo url('/'); ?>/js/custom.js"></script>
    <script src="<?php echo url('/'); ?>/js/jquery.vide.js"></script>


</body>
</html>

