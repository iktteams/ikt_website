<div id="mission" class="section wb">
      <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="message-box">
                    <br>
                    <h2>Our Mission</h2>
                    <p>IKTSS Software Solutions is one of the prominent software companies which helps other organizations to transform into digital enterprises and provides technical solutions.</p>
                    <p>
                       Our mission is to be a setup that provide the best software services globally.We inspires to serve humanity through skill development and
                       job creation.
                    </p>
                    <p>
                     Our vision is to nurturing the hidden potential and talent of our resources
                     and utilize the skills in coopeartion with the customer's requirements to attain success and creating long term relationship.
                    </p>

                    {{-- <a href="#contact" class="sim-btn hvr-bounce-to-top"><span>Contact Us</span></a> --}}
                    {{-- <a href="#contact" class="sim-btn js-scroll-trigger"><span>Contact Us</span></a> --}}
                </div><!-- end messagebox -->
            </div><!-- end col -->

            <div class="col-md-6">
                <div class="right-box-pro wow fadeIn">
                    <img src="uploads/our-vision.jpg" alt="" class="img-fluid img-rounded">
                </div><!-- end media -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</div><!-- end section -->

